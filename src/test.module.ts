import { testSchema } from './practice.dto';

import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {MongooseModule} from '@nestjs/mongoose';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'User', schema: testSchema }])    ],
  controllers: [AppController],
  providers: [AppService],
})
export class TestModule {}
