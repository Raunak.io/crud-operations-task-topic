import { PracticeDto } from './practice.dto';
import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
 async getAllData():Promise<any> {
   const data= await this.appService.getALL();
   return data;
  }


  @Get(':id')
  async getOne(@Param('id') dataId:string):Promise<PracticeDto>{

  const oneProd = await this.appService.getOne(dataId);

    console.log(`data id is ${dataId}`);
    return oneProd;
  }


  @Post()
  async createMethod(@Body() getData: PracticeDto):Promise< string> {
   await this.appService.create(getData);
    console.log(getData);
    return 'added data successfully';
  }

  @Patch(':id')
  async updateData(
    @Param('id') dataId: string,
    @Body() getDataUpdtn: PracticeDto,
  ): Promise<string> {

await this.appService.update(dataId,getDataUpdtn);

    console.log(
      `data with id ${dataId} with data ${getDataUpdtn} for updation`,
    );
    return 'data updated successfully';
  }

  @Delete(':id')
  async deleteData(@Param('id') dataId: string): Promise<string> {

 await this.appService.delete(dataId);

    console.log(`data with id ${dataId}`);

    return `data with id ${dataId} is deleted successfully`;
  }
}
