

import * as mongoose from 'mongoose';
export const testSchema = new mongoose.Schema({
 firstName: {
    type: String,
    required: true,
  },
 lastName: {
    type: String,
    required: true,
  },
 
});


export class PracticeDto extends mongoose.Document {
    firstName?:string;
    lastName?:string;
}