import { Injectable, NotFoundException } from '@nestjs/common';
import { PracticeDto } from './practice.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class AppService {
  constructor(
    @InjectModel('User') private readonly testModel: Model<PracticeDto>,
  ) {}

  async getALL(): Promise<PracticeDto[]> {
    const res = await this.testModel.find();
    return res;
  }
  async getOne(id: string): Promise<PracticeDto> {
    const res = await this.testModel.findById(id);
    console.log(res);
    return res;
  }
  async create(data: PracticeDto): Promise<PracticeDto> {
    const creation = new this.testModel(data);
    const res = await creation.save();
    console.log(res);
    return;
  }
  async update(id: string, data: PracticeDto): Promise<PracticeDto> {
    const updationData = await this.testModel.findOneAndUpdate({_id:id},data);
    updationData.save();
    console.log(data, id);
    return;
  }
  async delete(id: string): Promise<PracticeDto> {
    const result = await this.testModel.findByIdAndDelete(id);
    console.log(result);
    return;
  }

  private async findData(id: string): Promise<PracticeDto> {
    let details;
    try {
      details = await this.testModel.findById(id);
    } catch (error) {
      throw new NotFoundException('details not found .');
    }
    if (!details) {
      throw new NotFoundException('details not found .');
    }
    console.log(details);
    return details;
  }
}
